# DarkReads

Simple React application to keep track of personal readings. This project is an assignment for Udacity's React Fundamentals course.

## Installation

Requirements to run this project include [Node](https://nodejs.org) v6.0.0 or above. From within the project main folder:

1. Install all project's dependencies:

```bash
npm install
```

Go ahead and start the development server by running:

```bash
npm start
```

Open [http://localhost:3000]() to view it in the browser.

Inside `package.json` you'll find some additional scripts you can run to build assets, testing and deploying the application in production.

## User Guide

The project has been built from scratch, although closely following all requirements for the projects and indications provided as comments in the code inside the starter repository.

## Features

- The main page fetches data from the backend, storing all required pieces of state without manually setting predefined bookshelves. This makes it possible to dynamically build all necessary shelves and remove them from display as soon as a bookshelf is empty.
- The code has been structured in a number of components, although not every single component has already been isolated and wired up.
- A couple of external react libraries have been loaded for icons and a loading spinner indicator, to offer some nice feedback to the end user.
- Errors from the server should be handled correctly.
- Implemented two different versions for the `BooksAPI.update()` endpoint, one for each route, for the following reasons:
  - In the main `/` route state updates for `books` can happen in a simple way, by concatenating the updated `book` at the end of the `books` array.
  - In the `/search` route, an `update()` helper has been used to keep track of and return the updated book at the exact index in the array. This way the book wouldn't "jump" at the end of the query results after re-rendering.
- couple of helpers have been included in order to truncate excessively long authors lists, which wouldn't display nicely.
- To compensate for inaccurate informations, a modal has been added to display additional informations for each book.
- By hitting the `/search` route, the focus is immediately set to the input, so the end user just have to type his query.
- Search requests have been debounced to alleviate the server and frequent `403 forbidden` responses.
- A custom theme has been styled after the popular [Dracula theme](https://draculatheme.com) just for fun.

## ToDo

Following are some improvements as well as fixes that I'd like to implement in the near future, and on which I'd like to receive some advice by the Udacity code review team.

- [ ] Implement local storage to make the app more aware of its state. Currently, there's no way to let the app know which book has already been shelved by the current user, because the `BooksAPI.search()` endpoint doesn't return the shelf. The best one could do would be moving the state for books fetched upstream, on a parent component. Actually each route has it's own main component as this is more than sufficient to fulfill all functionalities. An added bonus of this setup is that each `props` has to travel one less level to reach its intended leaf component. Finally, there is no other way to hit directly the `/search` route with the app knowing already the state of each book already shelved. This make it impossible to add some nice additional UI features.
- [ ] Implement HOC in order to get rid of some nasty conditional logic which is currently polluting the code base. ADVICE APPRECIATED.
- [ ] Fix the bug in modals, that make it impossible to retrieve nested object properties. ADVICE APPRECIATED.
- [ ] Clean up the the code and standardize implementation of each method in the code base.
- [ ] Making styles fully responsive.
- [ ] Improve modals rendering for books with wordy descriptions.

## Contributing

I'd love to receive your feedback and help on DarkReads app! Feel free to open an issue if you find a bug. If you care to squash it yourself or discuss some new incredible features you'd like to see in an upcoming release, open a PR and I'll be right back 😃

## Acknowledgements

This project would not see the light of day without the awesome contributions of many incredible projects. Besides the usual suspects, including all the fine folks from [React][1], [and][2] [closely][3] [related][4] [projects][5] that make JavaScript and front-end development awesome, DarkReads relies directly on the following projects in order to work:

- [lodash][6]
- [react-feather][7]
- [react-modal][8]
- [react-spinners][9]

Dig into package.json file for a complete rundown and check them out!

[1]: https://facebook.github.io/react/
[2]: https://github.com/facebookincubator/create-react-app
[3]: https://reacttraining.com/react-router/
[4]: https://nodejs.org/en/
[5]: https://www.npmjs.com/
[6]: https://lodash.com/
[7]: https://github.com/carmelopullara/react-feather
[8]: https://github.com/reactjs/react-modal
[9]: https://github.com/davidhu2000/react-spinners
